#include "uploadfile.h"
#include "ui_uploadfile.h"
#include <QString>
#include <QMessageBox>
#include <QDebug>

UploadFile::UploadFile(QFile *infile, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UploadFile)
{
    ui->setupUi(this);
    this->inputfile = infile;
}

UploadFile::~UploadFile()
{
    delete ui;
}

QHttpPart part_parameter(QString key, QString value) {
    QHttpPart part;
    part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"" + key + "\""));
    part.setBody(value.toLocal8Bit());
    return part;
}

void UploadFile::uploadData() {
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    multiPart->append(part_parameter("country", ui->lineEdit_country->text().toUtf8()));
    multiPart->append(part_parameter("city", ui->lineEdit_city->text().toUtf8()));
    multiPart->append(part_parameter("street", ui->lineEdit_street->text().toUtf8()));
    QHttpPart modelPart;
    modelPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/octet-stream"));
    modelPart.setHeader(QNetworkRequest::ContentDispositionHeader,
                        QVariant("form-data; name=\"filename\"; filename=\"./" + this->inputfile->fileName() + "\""));

    QFile *file = new QFile(this->inputfile->fileName());
    file->open(QIODevice::ReadOnly);
    modelPart.setBodyDevice(file);
    file->setParent(multiPart);
    multiPart->append(modelPart);

    QUrl url("http://foxdie.pythonanywhere.com/add_file/");

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(get_reply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(QNetworkRequest(url), multiPart);
    multiPart->setParent(reply);

    qDebug() << "Transmitting" << file->size() << "bytes file.";

    QObject::connect(reply, SIGNAL(finished()), this, SLOT(get_reply()));
}

void UploadFile::get_reply(QNetworkReply *reply) {
    QVariant statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
    qDebug() << statusCode.isValid();
    qDebug() << statusCode.toInt();
    if (statusCode.toInt() == 200)
    {
        QMessageBox msgBox;
        msgBox.setText(("Файл отправлен!"));
        msgBox.exec();
    }
    qDebug() << reply->error();
}

void UploadFile::on_buttonBox_accepted()
{
    uploadData();
}
