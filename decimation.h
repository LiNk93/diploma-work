#ifndef DECIMATION_H
#define DECIMATION_H

#include <datatypes.h>
#include <QFile>

class Filter : public QObject
{
    Q_OBJECT
public:
    explicit Filter(QObject *parent = 0);
    double filter_btv(qint16 x); // фильтрация одного отсчета
    void testfilter(QFile *in);

private:
    quint16 fop; // частота опроса
    quint16 fs; // частота среза
    int norm_k, N; // нормирующий коэффициент и количество звеньев 2-го порядка
    std::vector<double> a, b, c;
    double d; // коэффициент фильтра
    std::vector<double> w0,w1,w2;// массивы промежуточных переменных фильтра
    double y1,y2,y3,y4; // выходные переменные фильтра

private:
    void calc_params();
    void step(quint8 k, double &x, double &y); // Реализация k-го звена 2-го порядка
};

class Decimation : public QObject
{
    Q_OBJECT
public:
    explicit Decimation(QObject *parent = 0);
    int getSamplescount(QFile *in);
    void change_header(vaw_header *h);
    Filter btv;

private:
    int Samplescount;    
};
#endif // DECIMATION_H
