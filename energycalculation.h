#ifndef ENERGYCALCULATION_H
#define ENERGYCALCULATION_H

#include <datatypes.h>
#include <QCoreApplication>
#include <QProgressBar>
#include <QFile>
#include <QDebug>

class EnergyCalculation : public QObject
{
    Q_OBJECT
public:
    explicit EnergyCalculation(quint16 size = 2048, quint8 delta = 1, QObject *parent = 0);
    double all_energy(std::vector<qint16> *v);
    std::vector<double> energy(std::vector<qint16> *v,QString filename = "");
    std::vector<acoustic_events> search_events(std::vector<double> *e, double threshold);    

private:
    std::vector<double> energy(QString filename, QProgressBar *pr);
    quint16 size; // размер отрезка
    quint8 delta; // шаг, на который сдвигаются отрезки
    double max_e;
    quint32 samplerate;
    double scale(double x);

signals:
    void value_changed(int);
};

#endif // ENERGYCALCULATION_H
