#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QTextStream>
#include "QFileDialog"
#include <decimation.h>
#include <classification.h>
#include "spectrogram.h"
#include "graphfordata.h"
#include "uploadfile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QFile *inputFile;
    QFile *outputFile;
    void decimate(QFile *f);

private slots:
    void on_pushButtonForGetFile_clicked();
    void on_getSpectrogram_clicked();
    void on_getGraph_clicked();
    void on_analyzeButton_clicked();
    void on_Upload_file_clicked();
    void on_Auto_clicked();

public slots:
    void enableSlots();

private:
    Ui::MainWindow *ui;
    int samplescount;
};

#endif // MAINWINDOW_H
