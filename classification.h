#ifndef CLASSIFICATION_H
#define CLASSIFICATION_H

#include <datatypes.h>

class Classification : public QObject
{
    Q_OBJECT
public:
    explicit Classification(std::vector<double> *energy, quint32 samplerate, quint8 delta = 1, QObject *parent = 0);
    double get_eventduration(acoustic_events *ae);
    double get_eventenergy(acoustic_events *ae);
    double get_averageenergy(acoustic_events *ae);
    quint8 classificatebytime(acoustic_events *ae, double time_tr);
    quint8 classificatebyenergy(acoustic_events *ae, double energy_tr);
    quint8 classificate(acoustic_events *ae, double time_tr, double energy_tr);

private:
    quint32 samplerate;
    quint8 delta;
    std::vector<double> *energy;

signals:

public slots:

};

#endif // CLASSIFICATION_H
