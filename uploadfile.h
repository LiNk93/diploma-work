#ifndef UPLOADFILE_H
#define UPLOADFILE_H

#include <QDialog>
#include <vawfile.h>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QHttpMultiPart>
#include <QHttpPart>

namespace Ui {
class UploadFile;
}

class UploadFile : public QDialog
{
    Q_OBJECT

public:
    explicit UploadFile(QFile *infile, QWidget *parent = 0);
    ~UploadFile();
    void uploadData();

private:
    Ui::UploadFile *ui;
    QFile *inputfile;

private slots:
    void get_reply(QNetworkReply *reply);
    void on_buttonBox_accepted();
};

#endif // UPLOADFILE_H
