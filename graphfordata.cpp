#include "graphfordata.h"
#include "ui_graphfordata.h"
#include <QTextStream>

#define e_delta 50

Graphfordata::Graphfordata(QFile *infile, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Graphfordata)
{
    ui->setupUi(this);
    this->inputfile = infile;
    max_energy = -1;
    getData();
    scene = new QGraphicsScene(ui->graphicsView);
    ui->graphicsView->setScene(scene);

    for (int i = 0; i < maxwidth; i+= 100)
    {
        QGraphicsLineItem *backliney = scene->addLine(i, 0 - 400, i, 600);
        backliney->setPen(QPen(Qt::gray));
    }
    for (int i = -400; i < 600; i += 100)
    {
        QGraphicsLineItem *backlinex = scene->addLine(0, i, maxwidth, i);
        backlinex->setPen(QPen(Qt::gray));
    }
    // Подпись  по горизонтали
    for (int i=0; i < maxwidth; i+=500)
    {
        QString time = QString::number((double)(i*e_delta/12000));
        QString cordtext = QString("time = " + time);
        QGraphicsTextItem *cordpoint = scene->addText(cordtext);
        cordpoint->setPos(i, 400);
    }
    // Подпись по вертикали
    int step = 100;
    for (int i = 300; i > -400; i -= 100)
    {
        QString energy = QString::number((double)(step));
        QString cordtext = QString("en = " + energy);
        QGraphicsTextItem *cordpoint = scene->addText(cordtext);
        cordpoint->setPos(0, i);
        step+=100;
    }

    QGraphicsPolygonItem *poly = scene->addPolygon(polygon, QPen(Qt::darkGreen));

    QGraphicsLineItem *line = scene->addLine(0, maxpoint.ry(), maxpoint.rx(), maxpoint.ry());
    line->setPen(QPen(Qt::red));

    QString time = QString::number(e_delta*maxpoint.rx()/12000);
    QString pointString = QString("time " + time + ",energy " + QString::number(max_energy));
    QGraphicsTextItem *maxPoint = scene->addText(pointString);
    maxPoint->setPos(maxpoint.rx(), maxpoint.ry()-20);

    QGraphicsLineItem *linefut = scene->addLine(0, 400, maxwidth, 400);
    linefut->setPen(QPen(Qt::blue));
    //Set-up the view

    //Use ScrollHand Drag Mode to enable Panning
    ui->graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
}

Graphfordata::~Graphfordata()
{
    delete ui;
}

void Graphfordata::getData() {
    VawFile wav(inputfile);
    wav.read_header();
    std::vector<qint16> data = wav.read_alldata();
    en = new EnergyCalculation(4096,e_delta);
    std::vector<double> energy = en->energy(&data,inputfile->fileName());
    vector<qint16>().swap(data);
    maxpoint = QPointF(0, 99999);
    maxwidth = energy.size()+1;
    polygon<<QPointF(0, 400);
    for(int x = 1; x < energy.size()+1; x++)
    {
        if (max_energy < energy[x-1])
        {
            max_energy = energy[x-1];
            maxpoint = QPointF(x, -1*energy[x-1] + 400);
        }
        polygon<<QPointF(x, -1*energy[x-1] + 400);
    }
    polygon<<QPointF(maxwidth, 400);
}

void Graphfordata::wheelEvent(QWheelEvent* event) {

    ui->graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    // Scale the view / do the zoom
    double scaleFactor = 2.15;
    if(event->delta() > 0) {
        // Zoom in
        ui->graphicsView->scale(scaleFactor, scaleFactor);
    } else {
        // Zooming out
        ui->graphicsView->scale(1.0 / scaleFactor, 1.0 / scaleFactor);
    }

    // Don't call superclass handler here
    // as wheel is normally used for moving scrollbars
}

//void Graphfordata::mousePressEvent(QMouseEvent *event)
//{
//    int x = event->pos().x();
//    int graphY = polygon.at(x).y();
//    QGraphicsLineItem *currentCordLine = scene->addLine(x, 400, x, graphY);
//    currentCordLine->setPen(QPen(Qt::red));
//}


