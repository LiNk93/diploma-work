#include "vawfile.h"

VawFile::VawFile(QFile *f, QObject *parent) :
    QObject(parent)
{
    this->inputfile = f;
}

void VawFile::set_infile(QFile *in)
{
    inputfile = in;
}

// Чтение заголовка WAV-файла
void VawFile::read_header()
{
    bool isopen = false;
    if(!this->inputfile->isOpen())
    {
        this->inputfile->open(QIODevice::ReadOnly);
        isopen = true;
    }
    this->inputfile->seek(0);
    this->inputfile->read(reinterpret_cast<char *>(&this->vawHeader), sizeof(vaw_header));
    if(isopen)
        this->inputfile->close();
}

// Чтение одного отсчета по номеру отсчета. (читается только левый канал)
qint16 VawFile::read_data(quint32 samplenumber)
{
    bool isopen = false;
    if(!this->inputfile->isOpen())
    {
        this->inputfile->open(QIODevice::ReadOnly);
        isopen = true;
    }
    this->inputfile->seek(samplenumber*vawHeader.blockAlign+sizeof(vawHeader));
    stereo_channels tmp;
    this->inputfile->read(reinterpret_cast<char *>(&tmp), sizeof(stereo_channels));
    if(isopen)
        this->inputfile->close();
    return tmp.leftch;
}

// Чтение одного отсчета по смещению относительно начала файла. (читается только левый канал)
qint16 VawFile::read_data(quint64 offset)
{
    bool isopen = false;
    if(!this->inputfile->isOpen())
    {
        this->inputfile->open(QIODevice::ReadOnly);
        isopen = true;
    }
    this->inputfile->seek(offset);
    stereo_channels tmp;
    this->inputfile->read(reinterpret_cast<char *>(&tmp), sizeof(stereo_channels));
    if(isopen)
        this->inputfile->close();
    return tmp.leftch;
}

// Чтение массива данных. startsample - отсчет, с которого начинается чтение.
// count - количетсво отсчетов, которое н адо считать.
// *v - ссылка на пустой вектор.
void VawFile::read_data(std::vector<qint16> *v, quint32 startsample, quint32 count)
{
    bool isopen = false;
    if(!this->inputfile->isOpen())
    {
        this->inputfile->open(QIODevice::ReadOnly);
        isopen = true;
    }
    this->inputfile->seek(startsample*vawHeader.blockAlign+sizeof(vawHeader));
    for(quint32 i = 0; i<count;i++)
    {
        if(this->vawHeader.numChannels==2)
        {
            stereo_channels data;
            this->inputfile->read(reinterpret_cast<char *>(&data), sizeof(data));
            v->push_back((qint16)(data.leftch+data.rightch)/2);
        }
        else
        {
            qint16 data;
            this->inputfile->read(reinterpret_cast<char *>(&data), sizeof(data));
            v->push_back(data);
        }
    }
    if(isopen)
        this->inputfile->close();
}

// Чтение всех данных файла в память
// Возвращаем вектор с данными
std::vector<qint16> VawFile::read_alldata()
{
    bool isopen = false;
    if(!this->inputfile->isOpen())
    {
        this->inputfile->open(QIODevice::ReadOnly);
        isopen = true;
    }
    this->inputfile->seek(sizeof(vawHeader));
    quint32 size = this->vawHeader.subchunk2Size/(this->vawHeader.bitsPerSample/8);
    std::vector<qint16> v;
    for(int i=0;i<size;i++)
    {
        qint16 tmp;
        this->inputfile->read(reinterpret_cast<char *>(&tmp),sizeof(tmp));
        if(this->vawHeader.numChannels==1)
            v.push_back(tmp);
        else
            if(i%2==0)
                v.push_back(tmp);
    }
    if(isopen)
        this->inputfile->close();
    return v;
}

// Размер данных
quint32 VawFile::get_datasize()
{
    return this->vawHeader.subchunk2Size;
}

// Частота дискретизации
quint32 VawFile::get_samplerate()
{
    return this->vawHeader.sampleRate;
}

// Количество каналов
quint8 VawFile::get_numchannels()
{
    return this->vawHeader.numChannels;
}

// Окно Хэмминга
float VawFile::hamming_window(quint16 iter, quint16 size)
{
    return (0.54-0.46*cos((2*M_PI*iter)/(size-1))); // or /size???
}

// Окно Ханна
float VawFile::hann_window(quint16 iter, quint16 size)
{
    return 0.5*(1-cos((2*M_PI*iter)/(size-1))); // or /size???
}

// Окно Блэкмана
float VawFile::blackman_window(quint16 iter, quint16 size)
{
    return ( 0.42-0.5*cos( (2*M_PI*iter)/(size-1) )+ // or /size???
            0.08*cos( (4*M_PI*iter)/(size-1) ) );
}
