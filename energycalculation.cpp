#include "energycalculation.h"

EnergyCalculation::EnergyCalculation(quint16 size, quint8 delta, QObject *parent) :
    QObject(parent)
{
    this->size = size;
    this->delta = delta;
    this->max_e = -1;
}

// Вычисление энергии всего сигнала. Для нормирования
// *v - вектор входных данных
double EnergyCalculation::all_energy(std::vector<qint16> *v)
{
    double res = 0;
    for(int i=0;i<v->size();i++)
        res+=pow(v->at(i), 2);
    return res;
}

// Вычисление энергии сигнала по отрезкам.
// Возвращает вектор значений нормированной энергии всего сигнала
// *v - вектор входных данных. k - коэффициент масшттабирования
std::vector<double> EnergyCalculation::energy(std::vector<qint16> *v, QString filename)
{    
    QProgressBar p;
    quint32 count = v->size()/ delta;
    p.setGeometry(600, 500, 300, 60); p.setFocus();
    p.setRange(0,count); p.setWindowFlags(Qt::SubWindow);
    p.setWindowModality(Qt::ApplicationModal); p.setAlignment(Qt::AlignHCenter);
    p.setWindowTitle("CALC ENERGY...");
    p.show();
    QFile in(filename); in.open(QIODevice::ReadOnly);
    vaw_header hdr;
    in.read(reinterpret_cast<char *>(&hdr), sizeof(vaw_header));
    in.close();
    samplerate = hdr.sampleRate;
    std::vector<double> tmp; // вектор энергии
    QString energyfile;
    energyfile = filename.replace(".wav", " "+QString::number(size)+ " "+
                 QString::number(delta)+ ".energy");
    // Если возможно, то читаем энергию из файла
    tmp = energy(energyfile, &p);
    if(tmp.size()>0)
        return tmp;
    // иначе высчитываем и записываем в файл
    double res, norm = this->all_energy(v);
    max_e = -1;
    int iter = 0;
    // начало сигнала
    for(int i=this->size/2; i<this->size; i+=delta)
    {
        res = 0;
        for(int j=0; j<i; j++)
            res+=pow(v->at(j),2);
        res/=this->size; res/=norm;
        if(max_e < res)
            max_e = res;
        tmp.push_back(res);        
        iter++;
        if(iter%(count/100)==0)
        {
            p.setValue(iter);
            QCoreApplication::processEvents();
        }
    }
    // основная часть сигнала
    int offset = v->size()-this->size+1;
    for(int i=0; i<offset; i+=delta)
    {
        res = 0;
        for(int j=0; j<this->size; j++)
            res+=pow(v->at(j+i),2);
        res/=this->size; res/=norm;
        if(max_e < res)
            max_e = res;
        tmp.push_back(res);    
        iter++;
        if(iter%(count/100)==0)
        {
            p.setValue(iter);
            QCoreApplication::processEvents();
        }
    }
    // конец сигнала
    for(int i=offset-1; i<v->size()-(this->size/2); i+=delta)
    {
        res = 0;
        for(int j=v->size()-1; j>i;j--)
            res+=pow(v->at(j),2);
        res/=this->size; res/=norm;
        if(max_e < res)
            max_e = res;
        tmp.push_back(res);        
        iter++;
        if(iter%(count/100)==0)
        {
            p.setValue(iter);
            QCoreApplication::processEvents();
        }
    }
    QFile enfile(energyfile);
    enfile.open(QIODevice::WriteOnly);
    for(int i=0;i<tmp.size();i++)
    {
        double t = scale(tmp[i]);
        tmp[i] = t;
        enfile.write(reinterpret_cast<char *>(&t),sizeof(double));
    }
    return tmp;
}

double EnergyCalculation::scale(double x)
{
    double res = (double)(x*390)/max_e;
    return res;
}

// Поиск акустических событий по энергии
// std::vector<double> *e - указатель навектор значений энергии
// double threshold - шумовой порог
// возвращает вектор найденных событий. Формат: начало-конец собития
std::vector<acoustic_events> EnergyCalculation::search_events(std::vector<double> *e, double threshold)
{
    std::vector<acoustic_events> res;
    bool find = false;
    acoustic_events tmp;
    for(int i=0;i<e->size();i++)
    {
        if(e->at(i)>threshold)
        {
            if(!find)
            {
                find = true;
                tmp.start = i*delta;
            }
        }
        else
        {
            if(find)
            {
                find = false;
                tmp.end = i*delta;
                res.push_back(tmp);
            }
        }
    }
    // "Склеивание" близких событий
    std::vector<acoustic_events> vae;
    acoustic_events ae;
    int i = 0;
    while(i<res.size())
    {
        ae.start = res[i].start; ae.end = res[i].end;
        i++;
        while((res[i].start-ae.end)<(int)0.1*samplerate/delta && i<res.size())
        {
            ae.end = res[i].end;
            i++;
        }
        vae.push_back(ae);
    }
    // Нарезка "длинных" событий
    res.clear();
    for(int i=0;i<vae.size();i++)
    {
        double t = (double)(vae[i].end - vae[i].start)/samplerate;
        if(t>2.7)
        {
            if(t<4.2)
            {
                acoustic_events ev;
                int index = (vae[i].end - vae[i].start+1)/2;
                ev.start = vae[i].start; ev.end = vae[i].start + index - 5;
                res.push_back(ev);
                ev.start = ev.end + 10; ev.end = vae[i].end;
                res.push_back(ev);
            }
            else
            {
                acoustic_events ev;
                int index = (vae[i].end - vae[i].start+1)/3;
                ev.start = vae[i].start; ev.end = vae[i].start + index - 5;
                res.push_back(ev);
                ev.start = ev.end + 10; ev.end = ev.start + index - 5;
                res.push_back(ev);
                ev.start = ev.end + 10; ev.end = vae[i].end;
                res.push_back(ev);
            }
        }
        else
            res.push_back(vae[i]);
    }
    return res;
}

std::vector<double> EnergyCalculation::energy(QString filename, QProgressBar *pr)
{
    QFile enfile(filename);
    std::vector<double> res;
    if(enfile.exists())
    {
        enfile.open(QIODevice::ReadOnly);
        int i = 0;
        while(!enfile.atEnd())
        {
            double en;
            enfile.read(reinterpret_cast<char*>(&en), sizeof(double));
            res.push_back(en);
            if(i%100==0)
            {
                pr->setValue(i);
                QCoreApplication::processEvents();
            }
            i++;
        }
        enfile.close();
    }
    return res;
}
