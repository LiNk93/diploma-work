#ifndef FFT_H
#define FFT_H

#include <datatypes.h>

class FFT : public QObject
{
    Q_OBJECT

public:
    FFT(quint16 fft_size = 2048);
    std::vector<complex> do_fft(std::vector<qint16> *v);
    int get_fftsize() {return this->fft_size;}

private:
    void culcw(void);
    void invers(std::vector<complex> *v);
    void basefft(complex *a,complex *b, complex *w);
    complex addc(complex *a,complex *b);
    complex subc(complex *a,complex *b);
    complex mulc(complex *a,complex *b);
    complex *w;
    qint64 L; // Номер этапа
    quint16 fft_size;
};

#endif // FFT_H
