#ifndef GRAPHFORDATA_H
#define GRAPHFORDATA_H

#include <QWidget>
#include <QPainter>
#include <QColor>
#include <QPoint>
#include <vawfile.h>
#include <energycalculation.h>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPolygonItem>
#include <QGraphicsLineItem>
#include <QGraphicsTextItem>
#include <QDebug>
#include <QScrollBar>
#include <QMouseEvent>
#include <QWheelEvent>

using namespace std;

namespace Ui {
class Graphfordata;
}

class Graphfordata : public QWidget
{
    Q_OBJECT

public:
    explicit Graphfordata(QFile *infile, QWidget *parent = 0);
    ~Graphfordata();
    void getData();
//    void mouseMoveEvent(QMouseEvent *event);
//    void mousePressEvent(QMouseEvent *event);
    EnergyCalculation *en;

private:
    Ui::Graphfordata *ui;
    QGraphicsScene *scene;
    QFile *inputfile;
    vector<QPoint> xyValues;
    QPointF maxpoint;
    qreal maxwidth;
    double max_energy;
    QPolygonF polygon;

protected:
//    void paintEvent(QPaintEvent *);
    virtual void wheelEvent(QWheelEvent* event);
};

#endif // GRAPHFORDATA_H
