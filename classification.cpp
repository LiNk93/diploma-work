#include "classification.h"

Classification::Classification(std::vector<double> *energy, quint32 samplerate, quint8 delta, QObject *parent) :
    QObject(parent)
{
    this->samplerate = samplerate;
    this->energy = energy;
    this->delta = delta;
}

double Classification::get_eventduration(acoustic_events *ae)
{
    double res = (double)(ae->end - ae->start + 1)/samplerate;
    return res;
}

double Classification::get_eventenergy(acoustic_events *ae)
{
    double res = 0;
    for(int i=(int)ae->start/delta;i<(int)ae->end/delta;i++)
        res+=energy->at(i);
    return res;
}

double Classification::get_averageenergy(acoustic_events *ae)
{
    double res = get_eventenergy(ae);
    return (double)(delta*res/(ae->end - ae->start + 1));
}

quint8 Classification::classificate(acoustic_events *ae, double time_tr, double energy_tr)
{
    double time = get_eventduration(ae);
    double event_en = get_eventenergy(ae);
    if(time > time_tr && event_en > energy_tr)
        return 1;
    else
        if(time < time_tr && event_en < energy_tr)
            return 0;
        else
        {
            if(time>0.4)
                return 1;
            return 0;
        }
}

quint8 Classification::classificatebytime(acoustic_events *ae, double time_tr)
{
    double time = get_eventduration(ae);
    if(time > time_tr)
        return 1;
    return 0;
}

quint8 Classification::classificatebyenergy(acoustic_events *ae, double energy_tr)
{
    double event_en = get_eventenergy(ae);
    if(event_en > energy_tr)
        return 1;
    return 0;
}
