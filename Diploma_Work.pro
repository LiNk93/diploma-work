#-------------------------------------------------
#
# Project created by QtCreator 2014-03-11T23:11:59
#
#-------------------------------------------------

QT       += core gui printsupport
QT       += multimedia
QT       += widgets

TARGET = Diploma_Work
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    fft.cpp \
    vawfile.cpp \
    energycalculation.cpp \
    spectrogram.cpp \
    graphfordata.cpp \
    decimation.cpp \
    classification.cpp \
    uploadfile.cpp

HEADERS  += mainwindow.h \
    fft.h \
    vawfile.h \
    energycalculation.h \
    spectrogram.h \
    graphfordata.h \
    decimation.h \
    datatypes.h \
    classification.h \
    uploadfile.h

FORMS    += mainwindow.ui \
    spectrogram.ui \
    graphfordata.ui \
    uploadfile.ui

OTHER_FILES += \
    stats.txt \
    Sounds.txt \
    Gisto_new.xlsx \
    Sonic_Wave.jpg

RESOURCES += \
    Images.qrc
