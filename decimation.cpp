#include "decimation.h"
#include <QTextStream>

Decimation::Decimation(QObject *parent) :
    QObject(parent)
{
}

// Количество отсчетов в исходном файле
int Decimation::getSamplescount(QFile *in)
{
    in->open(QIODevice::ReadOnly);
    vaw_header hdr;
    in->read(reinterpret_cast<char *> (&hdr),sizeof(vaw_header));
    Samplescount = hdr.subchunk2Size / hdr.blockAlign;
    in->close();
    return Samplescount;
}

// Изсенение заголовка
void Decimation::change_header(vaw_header *h)
{
    if(h->numChannels==2)
    {
        h->subchunk2Size /= 2;
        h->chunksize = (quint32)h->subchunk2Size+36;
        h->numChannels = (quint16)1;
        h->byteRate = (quint32)h->sampleRate*h->bitsPerSample*h->numChannels/8;
        h->blockAlign =(quint16)h->bitsPerSample*h->numChannels/8;
    }
    if(h->sampleRate == 48000)
    {
        h->subchunk2Size /= 4;
        h->sampleRate=12000;
        h->byteRate = (quint32)h->sampleRate*h->bitsPerSample*h->numChannels/8;
    }
}

Filter::Filter(QObject *parent) :
    QObject(parent)
{
    N = 4;fs = 6000;fop = 48000; norm_k = 2;
    a.reserve(N);b.reserve(N);c.reserve(N);
    w0.reserve(N);w1.reserve(N);w2.reserve(N);
    for(int i=0;i<N;i++)
    {
        w1[i] = 0;w2[i] = 0;
    }
    calc_params();
}

// Вычисление коэффициентов
void Filter::calc_params()
{
    //double t = 2*sin(M_PI*fs/fop)/cos(M_PI*fs/fop);
    double t = 2*tan((double)M_PI*fs/fop);
    d = t*t;
    std::vector<double> s(N), cs(N);
    QFile o("filter.txt");
    o.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream str(&o);
    str << t << "\t" << d << "\n";
    for(int i=0; i<N;i++)
    {
        double angle = (double)M_PI*(i+0.5)/N;
        cs[i] = -1*t*sin(angle);
        a[i] = 4*(1 + cs[i]) + d;
        b[i] = 2*d - 8;
        c[i] = 4*(1 - cs[i]) + d;
        str << angle << "\t" << cs[i] << "\t" << a[i] << "\t" << b[i] << "\t" << c[i] << "\n";
    }
    o.close();
}

// k-тое звено фильтра
inline void Filter::step(quint8 k, double &x, double &y)
{
    w0[k]=(double)(1.0*x-a[k]*w2[k]-b[k]*w1[k])/c[k];
    y = (double)d*(double)(w0[k]+w2[k]+2*w1[k]);
    w2[k]=(double)w1[k];
    w1[k]=(double)w0[k];
}

// Фильтрация отсчета x
double Filter::filter_btv(qint16 x)
{
    double tmp = x;
    step(0, tmp, y1);
    step(1, y1, y2);
    step(2, y2, y3);
    step(3, y3, y4);
    y4 = (double)norm_k*y4;
    return y4;
}

void Filter::testfilter(QFile *in)
{
    vaw_header h;
    in->open(QIODevice::ReadOnly);
    in->read(reinterpret_cast<char *>(&h), sizeof(vaw_header));
    QFile out("delta impuls filtered.wav");
    out.open(QIODevice::WriteOnly);
    out.write(reinterpret_cast<char *>(&h), sizeof(vaw_header));
    int sc = (int)h.subchunk2Size/h.blockAlign;
    for(int i=0; i<sc;i++)
    {
        qint16 x;
        in->read(reinterpret_cast<char *>(&x), sizeof(qint16));
        double y = filter_btv(x);
        x = (qint16)y;
        out.write(reinterpret_cast<char *>(&x), sizeof(qint16));
    }
    out.close(); in->close();
}
