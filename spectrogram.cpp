#include "spectrogram.h"
#include "ui_spectrogram.h"

Spectrogram::Spectrogram(QFile *infile, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Spectrogram)
{
    ui->setupUi(this);
    this->inputfile = infile;
    getData();

    QGraphicsScene *scene = new QGraphicsScene(ui->graphicsView);
    ui->graphicsView->setScene(scene);
    QPixmap pixmap(QPixmap::fromImage(*image));
    QGraphicsPixmapItem *qpixmap = new QGraphicsPixmapItem(pixmap);
    scene->addItem(qpixmap);
}

Spectrogram::~Spectrogram()
{
    delete ui;
}

void Spectrogram::getData() {
    VawFile objVawFile(this->inputfile);
    inputfile->open(QIODevice::ReadOnly);
    objVawFile.read_header();
    int x = 0, iter = 0;
    std::vector<qint16> data = objVawFile.read_alldata();
    FFT fft;
    image = new QImage((int)data.size(), (int)fft.get_fftsize()/2, QImage::Format_RGB888);
    std::vector<qint16> fft_data(fft.get_fftsize());
    for(int i=0;i<fft_data.size();i++)
    {
        fft_data[i] = data[i];
        iter++;
    }

    for(int i =0; i<data.size(); i+=10)
    {
        std::vector<complex> spectr = fft.do_fft(&fft_data);
        for(int j = 0; j<fft_data.size()/2; j++)
        {
            float ampl = spectr[j].amplitude();
            image->setPixel(x, (fft_data.size()/2-j), floattoQColor(ampl).rgba()); // задать цвет точки из РГБ
        }
        fft_data.erase(fft_data.begin(),fft_data.begin()+10);
        for(int k=0;k<10;k++)
        {
            fft_data.push_back(data[iter]);
            iter++;
        }
        x++;
    }
}

QColor Spectrogram::floattoQColor(float f)
{
    QColor color;
    color.setBlue(floor(f / 256.0 / 256.0));
    color.setGreen(floor((f - color.blue() * 256.0 * 256.0) / 256.0));
    color.setRed(floor(f - color.blue() * 256.0 * 256.0 - color.green() * 256.0));

    return color;
}

//void Spectrogram::paintEvent(QPaintEvent *) {
//    //QSize max = ui->widget->size();
//    QPainter painter(this); // Создаю объект "художника" (можно использовать this)
//    painter.setViewport(ui->drawZone->contentsRect());
//    QPixmap pixmap;
//    pixmap.fromImage(*image,Qt::AutoColor);
//    painter.drawPixmap(10, 10, pixmap);
//    //painter.drawImage(QPointF(10,10),*image);
//    update();
//}

