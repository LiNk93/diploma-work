#include <math.h>
#include <QObject>
#include <QString>
#include <vector>
#ifndef DATATYPES_H
#define DATATYPES_H

typedef struct
{
    char chunkid[4]; //RIFF
    quint32 chunksize; // оставшийся размер
    char format[4]; // WAVE
    char subchunk1Id[4]; // fmt
    quint32 subchunk1Size; // размер до конца секции
    quint16 audioFormat; // Аудио формат
    quint16 numChannels; // Количество каналов
    quint32 sampleRate; // Частота дискретизации
    quint32 byteRate; // Количество байт, переданных за секунду воспроизведения.
    quint16 blockAlign; // Количество байт для одного сэмпла, включая все каналы.
    quint16 bitsPerSample; // Количество бит в сэмпле.
    char subchunk2Id[4]; // data
    quint32 subchunk2Size; // Количество байт в области данных.
} vaw_header; // заголовок WAV-файла

typedef struct
{
    qint16 leftch;
    qint16 rightch;
} stereo_channels; // Стерео запись

typedef struct
{
    quint32 start; // номер начального отсчета
    quint32 end; // номер конечного отсчета
} acoustic_events; // акустическо событие

// Класс комплексного числа
class complex
{
public:
    complex(float R = 0, float I = 0) { Re = R; Im = I;}
    float amplitude() { return (float)sqrt(Re*Re+Im*Im); } //амплитуда
    float Re; // действительная часть
    float Im; // мнимая часть
};

#endif // DATATYPES_H
