#ifndef SPECTROGRAM_H
#define SPECTROGRAM_H

#include <QWidget>
#include <QPainter>
#include <QColor>
#include <QPoint>
#include <vawfile.h>
#include <QRgb>
#include <fft.h>
#include <QImage>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

using namespace std;

namespace Ui {
class Spectrogram;
}

class Spectrogram : public QWidget
{
    Q_OBJECT

public:
    explicit Spectrogram(QFile *infile, QWidget *parent = 0);
    ~Spectrogram();
    void getData();

private:
    Ui::Spectrogram *ui;
    QFile *inputfile;
    QColor floattoQColor(float f);
    //vector<QPoint> xyValues;
    //QPolygonF polygon;

    QImage *image;

protected:

};

#endif // SPECTROGRAM_H
