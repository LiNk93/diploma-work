#include "fft.h"

FFT::FFT(quint16 fft_size)
{
    this->fft_size = fft_size;
}

// Умножение комплексного числа
complex FFT::mulc(complex *a,complex *b)
{
    complex tmp;
    tmp.Re=(a->Re * b->Re) - (a->Im * b->Im);
    tmp.Im=(a->Re * b->Im) + (a->Im * b->Re);
    return tmp;
}

// Сложение комплексного числа
complex FFT::addc(complex *a,complex *b)
{
    complex tmp;
    tmp.Re=a->Re + b->Re;
    tmp.Im=a->Im + b->Im;
    return tmp;
}

// Вычитание комплексного числа
complex FFT::subc(complex *a,complex *b)
{
    complex tmp;
    tmp.Re=a->Re - b->Re;
    tmp.Im=a->Im - b->Im;
    return tmp;
}

void FFT::culcw(void)
{
    for(qint32 k=0; k<fft_size/2; k++)
    {
        w[k].Re=cos(2*M_PI*k/fft_size);
        w[k].Im=-sin(2*M_PI*k/fft_size);
    }
}

//Инвертирование
void FFT::invers(std::vector<complex> *v)
{
    quint32 ind[fft_size];
    quint32 mask=1,t1,t2;
    complex tmp;
    for (quint32 i=0; i<fft_size; i++)
    {
        ind[i]=0;
        t1=i;
        for (qint32 n=0; n<L; n++)
        {
            ind[i]=ind[i]<<1;
            t2=mask & t1;
            ind[i]=ind[i] | t2;
            t1=t1>>1;
        }
        if (ind[i]>i)
        {
            tmp= v->at(i);
            v->at(i)=v->at(ind[i]);
            v->at(ind[i])=tmp;
        }
    }
    delete [] ind;
}

//Базовый шаг БПФ
void FFT::basefft(complex *a,complex *b,complex *w)
{
    complex tmp=*a;
    *b=mulc(b,w);
    *a=addc(a,b);
    *b=subc(&tmp,b);
}

// Преобразование Фурье
std::vector<complex> FFT::do_fft(std::vector<qint16> *v)
{
    std::vector<complex> res;
    for(int i=0;i<fft_size;i++)
        res.push_back(complex(v->at(i)));
    L=log10(fft_size)/log10(2);
    w = new complex[fft_size/2];
    invers(&res);
    culcw();
    int dx,dn,x0,x1,nw;
    nw=0;
    for (int m=0; m<L; m++)
    {  // m-номер этапа
        dx=pow(2,m);
        dn=dx-1;
        x0=0;
        for (int n=0; n<fft_size/2; n++)
        {   // n-номер базовой операции
            x1=x0+dx;
            basefft(&res[x0],&res[x1],&w[nw]);
            if (dn)
            {
                dn--;
                x0++;
                nw=nw+pow(2,(L-m-1));
            }
            else
            {
                dn=dx-1;
                x0=x0+dx+1;
                nw=0;
            }
        }
    }
    delete [] w;
    return res;
}
