#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#include <QDir>
#include <QStringList>
#include <QMessageBox>

#define e_delta 50

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    inputFile = 0;
    outputFile = 0;
    QObject::connect(ui->pushButtonForGetFile, SIGNAL(clicked()), this, SLOT(enableSlots()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::enableSlots()
{
    if (inputFile) {
        ui->analyzeButton->setEnabled(true);
        ui->Upload_file->setEnabled(true);
        ui->getGraph->setEnabled(true);
        ui->getSpectrogram->setEnabled(false); // пока забанил
    }
    else
    {
        ui->analyzeButton->setEnabled(false);
        ui->Upload_file->setEnabled(false);
        ui->getGraph->setEnabled(false);
        ui->getSpectrogram->setEnabled(false); // пока забанил
    }
}

void MainWindow::decimate(QFile *f)
{
    f->open(QIODevice::ReadOnly);
    // Чтение заголовка
    vaw_header hdr;
    f->read(reinterpret_cast<char *>(&hdr), sizeof(vaw_header));
    // Вычисление количества сэмплов
    samplescount = hdr.subchunk2Size/hdr.blockAlign;
    // Флаги для конвертации в моно и децимации
    bool stereo = (hdr.numChannels==2), decim = (hdr.sampleRate==48000);
    outputFile->open(QIODevice::WriteOnly);
    // Прогрессбар
    QProgressBar p;
    p.setGeometry(600, 500, 300, 60); p.setFocus();
    p.setRange(0,samplescount); p.setWindowFlags(Qt::SubWindow);
    p.setWindowModality(Qt::ApplicationModal); p.setAlignment(Qt::AlignHCenter);
    p.setWindowTitle("DECIMATING...");
    p.show();
    Decimation dc(f);
    // Изменение заголовка и запись в выходной файл
    dc.change_header(&hdr);
    outputFile->write(reinterpret_cast<char *>(&hdr), sizeof(vaw_header));
    for(int i=0;i<samplescount;i++)
    {
        qint16 sample;
        if(stereo)
        { // Преобразование в моно
            stereo_channels sc;
            f->read(reinterpret_cast<char *>(&sc), sizeof(stereo_channels));
            sample = (qint16)(sc.leftch+sc.rightch)/2;
        }
        else
            f->read(reinterpret_cast<char *>(&sample), sizeof(qint16));
        if(decim)
        { // Децимация
            double ny = dc.btv.filter_btv(sample);
            sample = (qint16) ny;
            if(i%4==0) // Оставляем только каждый четвертый отсчет
                outputFile->write(reinterpret_cast<char *>(&sample), sizeof(qint16));
        }
        else
            outputFile->write(reinterpret_cast<char *>(&sample), sizeof(qint16));
        if(i%(samplescount/100)==0)
        {
            p.setValue(i);
            QCoreApplication::processEvents();
        }
    }
    f->close(); outputFile->close();
    p.close();
}

void MainWindow::on_pushButtonForGetFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                     "",
                                                     tr("Files (*.wav*)"));
    QFile *file = new QFile(fileName);
    if (file->exists())
    {
        delete inputFile;
        inputFile = file;
        ui->LabelForWay->setText(file->fileName());
        vaw_header h;
        inputFile->open(QIODevice::ReadOnly);
        inputFile->read(reinterpret_cast<char *>(&h), sizeof(vaw_header));
        inputFile->close();
        if(h.numChannels>1 || h.sampleRate == 48000)
        {
            QMessageBox::StandardButton reply =
               QMessageBox::question(this, "Конвертировать данные?",
               (QString)"Для повышения быстродействия и уменьшения потребления ресурсов "+
               (QString)"необходимо преобразовать данные в моно и уменьшить частоту "+
               (QString)"дискретизации. Конвертировать?", QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes)
            {
                QString s = inputFile->fileName();
                QFile *newfile = new QFile(s.replace(".wav", " DECIMATED.wav"));
                outputFile = newfile;
                decimate(inputFile);
                //Decimation dc;
                //dc.btv.testfilter(inputFile);
                QMessageBox::StandardButton repl =
                    QMessageBox::question(this, "Удалить файл?",
                    "Удалить исходный файл?", QMessageBox::Yes|QMessageBox::No);
                if (repl == QMessageBox::Yes)
                    inputFile->remove();
                delete inputFile; inputFile = outputFile;
                ui->LabelForWay->setText(inputFile->fileName());
            }
        }
        return;
    }
    delete file;    
}

void MainWindow::on_getSpectrogram_clicked()
{
    Spectrogram *SpectrWind = new Spectrogram(inputFile);
    SpectrWind->show();
}

void MainWindow::on_getGraph_clicked()
{    
    Graphfordata *GraphWind = new Graphfordata(inputFile);
    GraphWind->show();
}

void MainWindow::on_analyzeButton_clicked()
{
    VawFile f(inputFile);
    f.read_header();
    EnergyCalculation energy(4096,e_delta);
    std::vector<qint16> data = f.read_alldata();
    std::vector<double> energy_values = energy.energy(&data, inputFile->fileName());
    vector<qint16>().swap(data);
    // Ищем акустические события
    std::vector<acoustic_events> ac_events = energy.search_events(&energy_values,24);
    ui->EventsCount->setText(QString::number(ac_events.size()));
    // Классифицируем
    Classification cl(&energy_values,f.get_samplerate(),e_delta);
    std::vector<acoustic_events> cars;
    QString outname = inputFile->fileName();
    outname.replace(".wav", " EVENTS_STATS.txt");
    QFile out(outname);
    out.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream str(&out);
    for(int i=0;i<ac_events.size();i++)
    {
        if(cl.classificate(&ac_events[i],0.4,15000)==1)
        {
            cars.push_back(ac_events[i]);
            str << "car" << "\t";
        }
        else
            str << "\t";
        double time  = (double)ac_events[i].start/f.get_samplerate();
        str << time << "\t";
        time = (double)ac_events[i].end/f.get_samplerate();
        str << time << "\t" << cl.get_eventduration(&ac_events[i]) << "\t"
            << cl.get_eventenergy(&ac_events[i]) << "\t" << "\n";
    }
    ui->CarsCount->setText(QString::number(cars.size()));
    vector<double>().swap(energy_values);
    out.close();
}

void MainWindow::on_Upload_file_clicked()
{
    UploadFile *uploadWind = new UploadFile(inputFile);
    uploadWind->show();
}

void MainWindow::on_Auto_clicked()
{
    double min_t = ui->lineEdit_mintime->text().toDouble(),
           max_t = ui->lineEdit_maxtime->text().toDouble(),
           step_t = ui->lineEdit_steptime->text().toDouble(),
           min_e = ui->lineEdit_minenergy->text().toDouble(),
           max_e = ui->lineEdit_maxenergy->text().toDouble(),
           step_e = ui->lineEdit_stepenergy->text().toDouble();
    int size_t = ceil((max_t-min_t)/step_t);
    int size_e = ceil((max_e-min_e)/step_e);
    std::vector<quint16> time_stat(size_t,0);
    std::vector<quint16> energy_stat(size_e,0);
//    QString path = QDir::currentPath() + "/experiment/"; // так можно получить текущую директорию (в которой exe файл)
    QString path = "../experiment/";
    QString fileext = "*.wav";
    QDir currentDir = QDir(path);
    qDebug() << currentDir.absolutePath();
    QStringList files;
    files = currentDir.entryList(QStringList(fileext),
        QDir::Files | QDir::NoSymLinks);
    QFile *out_events = new QFile(path+"EVENTS.txt");
    out_events->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream str(out_events);
    int car_count = 0;
    foreach(QString filename,files)
    {
        QFile *in = new QFile(path+filename);
        VawFile f(in);
        f.read_header();
        if(f.get_numchannels()==2 || f.get_samplerate()==48000)
        {
            QString s = in->fileName();
            QFile *newfile = new QFile(s.replace(".wav", " DECIMATED.wav"));
            outputFile = newfile;
            decimate(in);
            delete in; in = outputFile;
            f.set_infile(in);
        }
        EnergyCalculation energy(4096,e_delta);
        std::vector<qint16> data = f.read_alldata();
        std::vector<double> energy_values = energy.energy(&data, in->fileName());
        vector<qint16>().swap(data);
        // Ищем акустические события
        std::vector<acoustic_events> ac_events = energy.search_events(&energy_values,24);
        Classification cl(&energy_values,f.get_samplerate(),e_delta);        
        str << filename << "\n";
        for(int i=0; i<ac_events.size();i++)
        {
            str << (double)ac_events[i].start/f.get_samplerate() << "\t" <<
                   (double)ac_events[i].end/f.get_samplerate() << "\t";
            double time = cl.get_eventduration(&ac_events[i]);
            str << time << "\t";
            double ener = cl.get_eventenergy(&ac_events[i]);
            str << ener <<"\n";
            if(time<=max_t && time>=min_t)
            {
                int index = floor((double)(time-min_t)/step_t);
                time_stat[index]++;
            }
            else
                if(time>max_t)
                    time_stat[time_stat.size()-1]++;
            if(ener<=max_e && ener>=min_e)
            {
                int index = floor((double)(ener-min_e)/step_e);
                energy_stat[index]++;
            }
            else
                if(ener>max_e)
                    energy_stat[energy_stat.size()-1]++;
            if(cl.classificate(&ac_events[i],0.4,15000)==1)
                car_count++;
        }
    }
    qDebug() << car_count;
    out_events->close();
    QFile *out_stats = new QFile(path+"STATS.txt");
    out_stats->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream strs(out_stats);
    for(int i=0; i<time_stat.size();i++)
        strs << time_stat[i] << "\t";
    strs << "\n";
    for(int i=0; i<energy_stat.size();i++)
        strs << energy_stat[i] << "\t";
    out_stats->close();
    QMessageBox msgBox;
    msgBox.setText(("Эксперимент проведен!\nОткройте файлы:\nEVENTS.txt\nSTATS.txt"));
    msgBox.exec();
}

