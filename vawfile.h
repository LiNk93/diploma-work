#ifndef VAWFILE_H
#define VAWFILE_H

#include <QFile>
#include <datatypes.h>

class VawFile : public QObject
{
    Q_OBJECT
public:
    explicit VawFile(QFile *f, QObject *parent = 0);
    void read_header();
    qint16 read_data(quint32 samplenumber);
    qint16 read_data(quint64 offset);
    void read_data(std::vector<qint16> *v, quint32 startsample, quint32 count);
    std::vector<qint16> read_alldata();
    quint32 get_datasize();
    quint32 get_samplerate();
    quint8 get_numchannels();
    void set_infile(QFile *in);

private:
    float hamming_window(quint16 iter, quint16 size); // Окно Хэмминга
    float hann_window(quint16 iter, quint16 size);// Окно Ханна
    float blackman_window(quint16 iter, quint16 size);//Окно Блэкмана
    vaw_header vawHeader;
    stereo_channels data;
    QFile *inputfile;

    Q_DISABLE_COPY(VawFile)
};

#endif // VAWFILE_H
